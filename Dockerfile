FROM openjdk:8
RUN apt-get update && apt-get install iputils-ping -y
ADD demo-1.0.0.jar app.jar
CMD ["--server.port=8088"]
EXPOSE 8088
ENTRYPOINT ["java","jar","/app.jar"]