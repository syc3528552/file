package com.syc.controller;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author ：SYC
 * @date ：Created in 2019/3/27 10:09
 * @description：
 * @modified By：
 */
@RestController
public class GoodsController {

    public int line = 1;

//    @Scheduled(cron = "0 0/30 * * * ? ")
//    @Scheduled(cron = "0 0/1 * * * ? ")
//    @Scheduled(cron = "0/3 * * * * ? ")
    public void testPic() {
        try{
            String userDir = System.getProperty("user.dir");
            String fg = System.getProperty("file.separator");
            String readFilePath = userDir + fg + "allToken.txt";
            File file = new File(readFilePath);
            String outFilePath=userDir + fg + "oneToken.txt";
            BufferedReader br = new BufferedReader(new FileReader(file));
            FileOutputStream outputStream = new FileOutputStream(outFilePath);
            OutputStreamWriter or = null;
            String s;
            int i = 0;
            while((s = br.readLine())!=null){
                i++;
                if(line == i) {
                    or = new OutputStreamWriter(outputStream);
                    or.write(s);
                    System.out.println("******"+ "第"+ line+ "行" +"*******：" +s);
                }
            }
            int lines = (int) Files.lines(Paths.get(readFilePath)).count();
            line++;
            if(line > lines) {
                line = 1;
            }
            br.close();
            or.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @RequestMapping("/test")
    public String test(){
        return "测试jenkins";
    }
}
